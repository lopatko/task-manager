package ru.volnenkoLopatko.spring.command.data.xml;

import org.springframework.context.annotation.Bean;

public class DataXmlConfig {
    @Bean
    public DataXmlClearCommand dataXmlClearCommand() {
        return new DataXmlClearCommand();
    }

    @Bean
    public DataXmlLoadCommand dataXmlLoadCommand() {
        return new DataXmlLoadCommand();
    }

    @Bean
    public DataXmlSaveCommand dataXmlSaveCommand() {
        return new DataXmlSaveCommand();
    }
}
