package ru.volnenkoLopatko.spring.command.data.json;

import org.springframework.context.annotation.Bean;

public class DataJsonConfig {
    @Bean
    public DataJsonClearCommand dataJsonClearCommand() {
        return new DataJsonClearCommand();
    }

    @Bean
    public DataJsonLoadCommand dataJsonLoadCommand() {
        return new DataJsonLoadCommand();
    }

    @Bean
    public DataJsonSaveCommand dataJsonSaveCommand() {
        return new DataJsonSaveCommand();
    }
}
