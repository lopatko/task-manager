package ru.volnenkoLopatko.spring.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.constant.DataConstant;
import ru.volnenkoLopatko.spring.entity.Domain;
import ru.volnenkoLopatko.spring.service.DomainService;

import java.io.File;
import java.nio.file.Files;

/**
 * @author Denis Volnenko
 */
public final class DataXmlSaveCommand extends AbstractCommand {

    @Autowired
    public DomainService domainService;

    @Override
    public String command() {
        return "data-xml-save";
    }

    @Override
    public String description() {
        return "Save Domain to XML.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML SAVE]");
        final Domain domain = new Domain();
        domainService.export(domain);
        final ObjectMapper objectMapper = new XmlMapper();
        final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        final String json = objectWriter.writeValueAsString(domain);
        final byte[] data = json.getBytes("UTF-8");
        final File file = new File(DataConstant.FILE_XML);
        Files.write(file.toPath(), data);
        System.out.println("[OK]");
    }

}
