package ru.volnenkoLopatko.spring.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.controller.InputProcessor;
import ru.volnenkoLopatko.spring.repository.ProjectRepository;

/**
 * @author Denis Volnenko
 */
public final class ProjectCreateCommand extends AbstractCommand {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    InputProcessor inputProcessor;

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = inputProcessor.nextLine();
        projectRepository.createProject(name);
        System.out.println("[OK]");
        System.out.println();
    }

}
