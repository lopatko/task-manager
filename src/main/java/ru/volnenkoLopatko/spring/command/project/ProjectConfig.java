package ru.volnenkoLopatko.spring.command.project;

import org.springframework.context.annotation.Bean;

public class ProjectConfig {
    @Bean
    public ProjectClearCommand projectClearCommand() {
        return new ProjectClearCommand();
    }

    @Bean
    public ProjectCreateCommand projectCreateCommand() {
        return new ProjectCreateCommand();
    }

    @Bean
    public ProjectListCommand projectListCommand() {
        return new ProjectListCommand();
    }

    @Bean
    public ProjectRemoveCommand projectRemoveCommand() {
        return new ProjectRemoveCommand();
    }
}
