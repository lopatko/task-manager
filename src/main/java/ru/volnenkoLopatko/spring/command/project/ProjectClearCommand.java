package ru.volnenkoLopatko.spring.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.repository.ProjectRepository;

/**
 * @author Denis Volnenko
 */
public final class ProjectClearCommand extends AbstractCommand {

    @Autowired
    ProjectRepository projectRepository;

    @Override
    public String command() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        projectRepository.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

}
