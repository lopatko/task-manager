package ru.volnenkoLopatko.spring.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.entity.Project;
import ru.volnenkoLopatko.spring.repository.ProjectRepository;
import ru.volnenkoLopatko.spring.service.ProjectService;

/**
 * @author Denis Volnenko
 */
public final class ProjectListCommand extends AbstractCommand {

    @Autowired
    private ProjectService projectService;

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        int index = 1;
        for (Project project: projectService.getListProject()) {
            System.out.println(index++ + ". " + project.getName());
        }
        System.out.println();
    }

}
