package ru.volnenkoLopatko.spring.command.system;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.controller.InputProcessor;

import java.util.List;

/**
 * @author Denis Volnenko
 */
public final class HelpCommand extends AbstractCommand {

    @Autowired
    private InputProcessor inputProcessor;

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (AbstractCommand command: inputProcessor.getCommandList()) {
            System.out.println(command.command()+ ": " + command.description());
        }
    }

}
