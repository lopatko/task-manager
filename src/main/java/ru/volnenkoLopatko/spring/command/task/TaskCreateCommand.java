package ru.volnenkoLopatko.spring.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.controller.InputProcessor;
import ru.volnenkoLopatko.spring.repository.TaskRepository;

/**
 * @author Denis Volnenko
 */
public final class TaskCreateCommand extends AbstractCommand {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private InputProcessor inputProcessor;

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = inputProcessor.nextLine();
        taskRepository.createTask(name);
        System.out.println("[OK]");
        System.out.println();
    }
}
