package ru.volnenkoLopatko.spring.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.repository.TaskRepository;

/**
 * @author Denis Volnenko
 */
public final class TaskClearCommand extends AbstractCommand {

    @Autowired
    TaskRepository taskRepository;

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public void execute() {
        taskRepository.clear();
        System.out.println("[ALL TASK REMOVED]");
    }

}
