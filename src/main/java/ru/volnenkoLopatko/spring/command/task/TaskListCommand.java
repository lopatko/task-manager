package ru.volnenkoLopatko.spring.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.entity.Task;
import ru.volnenkoLopatko.spring.repository.TaskRepository;

/**
 * @author Denis Volnenko
 */
public final class TaskListCommand extends AbstractCommand {

    @Autowired
    TaskRepository taskRepository;

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        int index = 1;
        for (Task task: taskRepository.getListTask()) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println();
    }

}
