package ru.volnenkoLopatko.spring.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import ru.volnenkoLopatko.spring.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;

public class TaskConfig {

    @Bean
    public TaskClearCommand taskClearCommand() {
        return new TaskClearCommand();
    }

    @Bean
    public TaskCreateCommand taskCreateCommand() {
        return new TaskCreateCommand();
    }

    @Bean
    public TaskListCommand taskListCommand() {
        return new TaskListCommand();
    }

    @Bean
    public TaskRemoveCommand taskRemoveCommand() {
        return new TaskRemoveCommand();
    }

}
