package ru.volnenkoLopatko.spring.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.controller.InputProcessor;

/**
 * @author Denis Volnenko
 */
public final class TaskRemoveCommand extends AbstractCommand {

    @Autowired
    InputProcessor inputProcessor;

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVING TASK]");
        System.out.println("Enter task order index:");
        final Integer orderIndex = inputProcessor.nextInteger();
        if (orderIndex == null) {
            System.out.println("Error! Incorrect order index...");
            System.out.println();
            return;
        }
        System.out.println("[OK]");
    }

}
