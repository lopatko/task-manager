package ru.volnenkoLopatko.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import ru.volnenkoLopatko.spring.command.data.bin.DataBinConfig;
import ru.volnenkoLopatko.spring.command.data.bin.DataBinaryClearCommand;
import ru.volnenkoLopatko.spring.command.data.bin.DataBinaryLoadCommand;
import ru.volnenkoLopatko.spring.command.data.bin.DataBinarySaveCommand;
import ru.volnenkoLopatko.spring.command.data.json.DataJsonClearCommand;
import ru.volnenkoLopatko.spring.command.data.json.DataJsonConfig;
import ru.volnenkoLopatko.spring.command.data.json.DataJsonLoadCommand;
import ru.volnenkoLopatko.spring.command.data.json.DataJsonSaveCommand;
import ru.volnenkoLopatko.spring.command.data.xml.DataXmlClearCommand;
import ru.volnenkoLopatko.spring.command.data.xml.DataXmlConfig;
import ru.volnenkoLopatko.spring.command.data.xml.DataXmlLoadCommand;
import ru.volnenkoLopatko.spring.command.data.xml.DataXmlSaveCommand;
import ru.volnenkoLopatko.spring.command.project.*;
import ru.volnenkoLopatko.spring.command.system.SystemConfig;
import ru.volnenkoLopatko.spring.command.task.*;
import ru.volnenkoLopatko.spring.controller.InputProcessor;

@ComponentScan
public class App {

    private static final Class[] classes = {
            ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectListCommand.class, ProjectRemoveCommand.class,
            TaskClearCommand.class, TaskCreateCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class,
            DataBinaryLoadCommand.class, DataBinarySaveCommand.class,
            DataBinaryClearCommand.class, DataJsonSaveCommand.class,
            DataJsonLoadCommand.class, DataJsonClearCommand.class,
            DataXmlClearCommand.class, DataXmlLoadCommand.class,
            DataXmlSaveCommand.class
    };

    public static final Class[] configs = {
            DataBinConfig.class,
            DataJsonConfig.class,
            DataXmlConfig.class,
            ProjectConfig.class,
            SystemConfig.class,
            TaskConfig.class,
            App.class
    };

    public static void main(String[] args) throws Exception {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(configs);
        context.getBean(InputProcessor.class).start();

    }

}
