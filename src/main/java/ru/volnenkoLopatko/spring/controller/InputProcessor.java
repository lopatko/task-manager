package ru.volnenkoLopatko.spring.controller;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import ru.volnenkoLopatko.spring.App;
import ru.volnenkoLopatko.spring.command.AbstractCommand;
import ru.volnenkoLopatko.spring.command.data.bin.DataBinaryClearCommand;
import ru.volnenkoLopatko.spring.command.data.bin.DataBinaryLoadCommand;
import ru.volnenkoLopatko.spring.command.data.bin.DataBinarySaveCommand;
import ru.volnenkoLopatko.spring.command.data.json.DataJsonClearCommand;
import ru.volnenkoLopatko.spring.command.data.json.DataJsonLoadCommand;
import ru.volnenkoLopatko.spring.command.data.json.DataJsonSaveCommand;
import ru.volnenkoLopatko.spring.command.data.xml.DataXmlClearCommand;
import ru.volnenkoLopatko.spring.command.data.xml.DataXmlLoadCommand;
import ru.volnenkoLopatko.spring.command.data.xml.DataXmlSaveCommand;
import ru.volnenkoLopatko.spring.command.project.ProjectClearCommand;
import ru.volnenkoLopatko.spring.command.project.ProjectCreateCommand;
import ru.volnenkoLopatko.spring.command.project.ProjectListCommand;
import ru.volnenkoLopatko.spring.command.project.ProjectRemoveCommand;
import ru.volnenkoLopatko.spring.command.system.HelpCommand;
import ru.volnenkoLopatko.spring.command.task.TaskClearCommand;
import ru.volnenkoLopatko.spring.command.task.TaskCreateCommand;
import ru.volnenkoLopatko.spring.command.task.TaskListCommand;
import ru.volnenkoLopatko.spring.command.task.TaskRemoveCommand;
import ru.volnenkoLopatko.spring.entity.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

@Controller
public class InputProcessor  {

    @Autowired
    private DataBinaryClearCommand dataBinaryClearCommand;

    @Autowired
    private DataBinaryLoadCommand dataBinaryLoadCommand;

    @Autowired
    private DataBinarySaveCommand dataBinarySaveCommand;

    //

    @Autowired
    private DataJsonClearCommand dataJsonClearCommand;

    @Autowired
    private DataJsonLoadCommand dataJsonLoadCommand;

    @Autowired
    private DataJsonSaveCommand dataJsonSaveCommand;

    //

    @Autowired
    private DataXmlClearCommand dataXmlClearCommand;

    @Autowired
    private DataXmlLoadCommand dataXmlLoadCommand;

    @Autowired
    private DataXmlSaveCommand dataXmlSaveCommand;

    //

    @Autowired
    private ProjectClearCommand projectClearCommand;

    @Autowired
    private ProjectCreateCommand projectCreateCommand;

    @Autowired
    private ProjectListCommand projectListCommand;

    @Autowired
    private ProjectRemoveCommand projectRemoveCommand;

    //

    @Autowired
    private HelpCommand helpCommand;

    //

    @Autowired
    private TaskClearCommand taskClearCommand;

    @Autowired
    private TaskCreateCommand taskCreateCommand;

    @Autowired
    private TaskListCommand taskListCommand;

    @Autowired
    private TaskRemoveCommand taskRemoveCommand;

    private final Scanner scanner = new Scanner(System.in);

    private List<AbstractCommand> commandList = new ArrayList<>();

    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

    public void start() throws Exception {

        fillCommandList();

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";

        while (!"exit".equals(command)) {
            command = scanner.nextLine();

            int index = searchCommand(command);

            if (index != -1) {
                AbstractCommand abstractCommand = commandList.get(index);
                abstractCommand.execute();
            }
        }
    }

    public String nextLine() {
        return scanner.nextLine();
    }

    public Integer nextInteger() {
        final String value = nextLine();
        if (value == null || value.isEmpty()) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return null;
        }
    }

    private void fillCommandList() {
        commandList.addAll(Arrays.asList(dataBinaryClearCommand, dataBinaryLoadCommand, dataBinarySaveCommand));
        commandList.addAll(Arrays.asList(dataJsonClearCommand, dataJsonLoadCommand, dataJsonSaveCommand));
        commandList.addAll(Arrays.asList(dataXmlClearCommand, dataXmlLoadCommand, dataXmlSaveCommand));
        commandList.addAll(Arrays.asList(
                projectClearCommand, projectCreateCommand, projectListCommand, projectRemoveCommand));
        commandList.addAll((Arrays.asList(helpCommand)));
        commandList.addAll(Arrays.asList(taskClearCommand, taskCreateCommand, taskListCommand, taskRemoveCommand));
    }

    private int searchCommand(String commandName) {
        for (int i = 0; i < commandList.size(); i++) {
            if (commandList.get(i).command().equals(commandName)) {
                return i;
            }
        }

        return -1;
    }
}
