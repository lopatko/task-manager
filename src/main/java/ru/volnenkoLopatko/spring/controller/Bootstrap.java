package ru.volnenkoLopatko.spring.controller;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.volnenkoLopatko.spring.api.repository.IProjectRepository;
import ru.volnenkoLopatko.spring.api.repository.ITaskRepository;
import ru.volnenkoLopatko.spring.api.service.IDomainService;
import ru.volnenkoLopatko.spring.api.service.IProjectService;
import ru.volnenkoLopatko.spring.api.service.ITaskService;
import ru.volnenkoLopatko.spring.api.service.ServiceLocator;
    import ru.volnenkoLopatko.spring.error.CommandAbsentException;
import ru.volnenkoLopatko.spring.error.CommandCorruptException;
import ru.volnenkoLopatko.spring.repository.ProjectRepository;
import ru.volnenkoLopatko.spring.repository.TaskRepository;
import ru.volnenkoLopatko.spring.service.DomainService;
import ru.volnenkoLopatko.spring.service.ProjectService;
import ru.volnenkoLopatko.spring.service.TaskService;
import ru.volnenkoLopatko.spring.command.AbstractCommand;

import java.util.*;

/**
 * @author Denis Volnenko
 */
public final class Bootstrap implements ServiceLocator {

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);

    private final IDomainService domainService = new DomainService();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();



    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IDomainService getDomainService() {
        return domainService;
    }


}
