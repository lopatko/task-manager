package ru.volnenkoLopatko.spring.api.service;

import ru.volnenkoLopatko.spring.entity.Domain;

/**
 * @author Denis Volnenko
 */
public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
